import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PricesComponent } from './components/prices/prices.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { SundayComponent } from './components/prices/usuario20/sunday/sunday.component';
import { MondayComponent } from './components/prices/usuario20/monday/monday.component';
import { TuesdayComponent } from './components/prices/usuario20/tuesday/tuesday.component';
import { Usuario20Component } from './components/prices/usuario20/usuario20.component';
import { Usuario50Component } from './components/prices/usuario50/usuario50.component';
import { Usuario80Component } from './components/prices/usuario80/usuario80.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    PricesComponent,
    HomeComponent,
    AboutComponent,
    SundayComponent,
    MondayComponent,
    TuesdayComponent,
    Usuario20Component,
    Usuario50Component,
    Usuario80Component
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
