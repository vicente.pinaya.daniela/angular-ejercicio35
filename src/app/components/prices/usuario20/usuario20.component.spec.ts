import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Usuario20Component } from './usuario20.component';

describe('Usuario20Component', () => {
  let component: Usuario20Component;
  let fixture: ComponentFixture<Usuario20Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Usuario20Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Usuario20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
